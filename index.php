<?php
    // $hello="Fundamental PHP 2"; //variable declaration, automatically recognize declaration type
    // $integer=50;
    // $string="Hello";
    // $boolean=true;
    // $float=34.7;
    // $null=NULL;
    // echo "Fundamental PHP";
    // echo "<br/>";
    // echo $hello;
    // echo "<br/>";
    // echo strlen($hello); //character count
    // echo "<br/>";
    // echo str_word_count($hello); //word count
    // echo "<br/>";
    // echo strrev($hello); //reverse character placement
    // echo "<br/>";
    // echo ("integer=".$integer);
    // echo "<br/>";
    // echo ("string=".$string);
    // echo "<br/>";
    // echo ("string=".$boolean);
    // echo "<br/>";
    // echo ("null=".$null);
    // echo "<br/>";
    // echo ("float=".$float);
    // echo "<br/>";
    // define("greeting","Hello Guys!");
    // echo greeting;
    // $angka = 1000;
    // $angka = $angka * 50;
    // echo ($angka);
    // echo "<br/>";

    // //condition if
    // $nilai = 75;
    // $abstain = 5;
    // if ($nilai > 60 && $abstain<=3)
    // {
    //     $status = "lulus";
    // }else{
    //     $status = "tidak lulus";    
    // }
    // echo ($status);
    // echo "<br/>";

    // //case
    // $skor = 3;
    // switch ($skor)
    // {
    //     case 5:
    //     $rating="baik sekali";
    //     break;
    //     case 4:
    //     $rating="baik";
    //     break;
    //     case 2:
    //     $rating="cukup";
    //     break;
    //     default:
    //     $rating="Tidak Valid";
    //     break;
    // }
    // echo ($rating);
    // echo "</br>";
    // for ($i=1; $i<=3; $i++)
    // {
    //     echo $i."</br>";
    // }
    // while ($i>0)
    // {
    //     echo "Hello"."</br>";
    //     $i--;
    // } 
    // $x = 0;    
    // $y = 1;
    // $z = 1;
    // for($i=0;$i<=5;$i++)    
    // {            
    //     echo $z."</br>";
    //     $z = $x + $y;                 
    //     $x=$y;       
    //     $y=$z;     
    // }
    
    // //function
    // function CtoF($tempC, $type)
    // {
    //     switch ($type)
    //     {
    //         case "F":
    //         $tempF=($tempC*9/5)+32;
    //         return $tempF;
    //         break;
    //         case "R":
    //         $tempR=$tempC*4/5;
    //         return $tempR;
    //         break;
    //         case "K":
    //         $tempK=$tempC+273;
    //         return $tempK;
    //         break;
    //         default:
    //         break;
    //     }
    // } 
    // $tempC = 38;
    // $type = "F";
    // echo "Human body temperature is ".$tempC."C or ".CtoF($tempC,$type).$type;
    // echo "</br>";
    // $days = array("Monday","Tuesday","Wednesday");
    // echo $days[2];
    // echo "</br>";
    // var_dump($days);
    // echo "</br>";

    // //array
    // $days2 = array
    // (
    //     "Senin"=>"Monday",
    //     "Selasa"=>"Tuesday",
    //     "Wednesday"=>"Rabu"
    // );
    // foreach ($days2 as $key=>$day)
    // {
    //     echo $key."-".$day."</br>";
    // }
    // $days2[]="Saturday";
    // $days2["Kamis"]="Thursday";
    // $days2["Jumat"]="Friday";
    // array_push($days2,"Sunday");
    // var_dump($days2);
    // echo "</br>";
    // $search=array_search("Saturday",$days2);
    // echo ($search);

    // //Class
    // class Animal
    // {
    //     private $food="Herbivore"; //private cannot be edited
    //     public $feet;
    //     public function __construct($feet)
    //     {
    //         $this->feet=$feet;
    //     }
    //     public function sounds()
    //     {
    //         return "Meow";
    //     }
    //     public function getfood() //Getter makes private into public
    //     {
    //         return $this->food;
    //     }
    //     public function setfood($newfood) //Setter allows setting private property
    //     {
    //         $this->food=$newfood;
    //     }
    // }
    // $cat = new Animal(4);
    // var_dump ($cat);
    // echo "</br>";
    // echo $cat->sounds();
    // echo "</br>";
    // echo $cat->getFood();
    // echo "</br>";
    // $cat->feet = 5; //public can be edited directly
    // echo $cat->feet;
    // echo "</br>";
    // $cat->setFood("Omnivore");
    // echo $cat->getFood();
    
    //Class Inheritance
    class Animal
    {
        public $food="Herbivore";
        public $feet="4";
        public function __construct($feet)
        {
            $this->feet=$feet;
        }
        public function sounds()
        {
            return "Meow";
        }
    }
    class Mammal extends Animal
    { 
        public $coating="hair";
    }
    $cat = new Mammal(4);
    echo $cat->feet;
    echo $cat->sounds();
    echo $cat->coating;
    echo "</br>";
    class Reptile extends Animal
    { 
        public $coating="scales";
    }
    $snake = new Reptile(4);
    echo $snake->feet;
    echo $snake->sounds();
    echo $snake->coating;
?>